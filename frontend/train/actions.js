import { createRoutine } from 'actions'

export const train = createRoutine('train/TRAIN')
export const loadLogs = createRoutine('train/LOGS')

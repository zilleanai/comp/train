import { call, put, takeLatest } from 'redux-saga/effects'

import { train } from 'comps/train/actions'
import { createRoutineFormSaga } from 'sagas'
import TrainApi from 'comps/train/api'


export const KEY = 'train'

export const trainSaga = createRoutineFormSaga(
  train,
  function* successGenerator(payload) {
    const response = yield call(TrainApi.train, payload)
    yield put(train.success(response))
  }
)

export default () => [
  takeLatest(train.TRIGGER, trainSaga)
]

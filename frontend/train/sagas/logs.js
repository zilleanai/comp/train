import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { loadLogs } from 'comps/train/actions'
import TrainApi from 'comps/train/api'
import { selectLogs } from 'comps/train/reducers/logs'


export const KEY = 'train_logs'

export const maybeLoadLogsSaga = function* ({ payload: project }) {
  const { byId, isLoading } = yield select(selectLogs)

  const isLoaded = !!byId[project.project]
  if (!(isLoaded || isLoading)) {
    yield put(loadLogs.trigger(project))
  }
}

export const loadLogsSaga = createRoutineSaga(
  loadLogs,
  function* successGenerator(project) {
    const logs = yield call(TrainApi.loadLogs, project)
    yield put(loadLogs.success(logs))
  }
)

export default () => [
  takeEvery(loadLogs.MAYBE_TRIGGER, maybeLoadLogsSaga),
  takeLatest(loadLogs.TRIGGER, loadLogsSaga),
]

import React from 'react'
import { compose } from 'redux'
import { storage } from 'comps/project'
import { v1 } from 'api'

import './tensorboard.scss'

class Tensorboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0
    };
  }

  render() {
    return (
      <div className="Tensorboard">
        <img onClick={()=>{this.setState({counter: this.state.counter+1})}} src={v1(`/train/tensorboard/${storage.getProject()}?counter=${this.state.counter}`)} />
      </div>
    );
  }
}

export default compose(
)(Tensorboard)

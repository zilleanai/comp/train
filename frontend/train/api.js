import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function trainUri(uri) {
  return v1(`/train${uri}`)
}

export default class Train {

  static train() {
    return post(trainUri(`/train/${storage.getProject()}`), {})
  }

  static loadLogs(project) {
    return get(trainUri(`/logs/${project}`))
  }

}

import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from werkzeug.utils import secure_filename
from ..tasks import train_async_task

from zworkflow import Config
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from Tensorboard2Seaborn import plot

class TrainController(Controller):

    @route('/train/<string:project>', methods=['POST'])
    def train(self, project):
        train_async_task.delay(project)
        return jsonify(success=True)

    @route('/status/<string:project>', methods=['GET'])
    def status(self, project):
        return jsonify(success=True)

    @route('/tensorboard/<string:project>', methods=['GET'])
    def tensorboard(self, project):
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        configfile = os.path.join(BASE_DIR, 'workflow.yml')
        config = Config(configfile)
        logdir = os.path.join(BASE_DIR, config['train']['tensorboard'])
        figures = plot(logdir=logdir)
        fig = figures['data/loss']
        output = BytesIO()
        FigureCanvas(fig).print_png(output)
        response = make_response(output.getvalue())
        response.headers.set('Content-Type', 'image/png')
        response.headers.set(
            'Content-Disposition', 'attachment', filename='%s.png' % '0')
        return response

    @route('/logs/<string:project>', methods=['GET'])
    def logs(self, project):
        redis = AppConfig.SESSION_REDIS
        key = 'train_logger_'+project
        logged = redis.get(key) or "{\"logs\":[]}"
        logged = json.loads(logged)
        return jsonify(project=project, logs=logged['logs'])

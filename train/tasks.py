import os
import gc
import json
import urllib
from io import StringIO, BytesIO
import pickle
import multiprocessing

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from zworkflow import Config
from zworkflow.dataset import get_dataset
from zworkflow.model import get_model
from zworkflow.preprocessing import get_preprocessing
from zworkflow.train import get_train


def train_logger(project, *args, **kwargs):
    redis = AppConfig.SESSION_REDIS
    key = 'train_logger_'+project
    logged = redis.get(key) or "{\"logs\":[]}"
    logged = json.loads(logged)
    mapped = "".join(map(str, args))
    logged['logs'].append(mapped)
    redis.setex(key, 600, json.dumps(logged))


@celery.task(serializer='pickle')
def train_async_task(project):
    multiprocessing.current_process()._config['daemon'] = False
    os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
    configfile = os.path.join('workflow.yml') or {}
    config = Config(configfile)
    config['general']['verbose'] = True

    preprocessing = get_preprocessing(config)
    dataset = get_dataset(config, preprocessing)
    if config['general']['verbose']:
        print(dataset)
    model = get_model(config)
    if config['general']['verbose']:
        print(model)
    logger = lambda *args, **kwargs: train_logger(project, args, kwargs)
    train = get_train(config)
    if config['general']['verbose']:
        print(train)
    train.train(dataset, model, logger=logger)
